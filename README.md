# Sylius Test Suite - Cypress
An end-to-end test suite to Sylius showcase with Cypress.

You can install the sylius showcase with one docker container with this repository: https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-showcase

**We have 200 test cases implemented with Cypress.**

In the `original` directory we have the test cases implemented by computer science students. The tests passed in sylius-showcase without delay.

## Reduced version

**We have 10 test cases implemented with Cypress.**

In the `static-wait` directory we have the test cases with the refactor added static wait in places impacted by the sync problem.

In the `explicit-wait` directory we have the test cases with the refactor added explicit wait in places impacted by the sync problem.

In the `wait-network` directory we have the test cases with the refactor added wait network in places impacted by the sync problem.

In the `stable-dom` directory we have the test cases with the refactor added stable DOM wait in places impacted by the sync problem.

## Execute

First install the dependencies:

```
yarn install
```

**For execute the cypress scripts** you can use the cypress interface with:

```
yarn cypress
```

You can also run an unique test file in terminal:

```
yarn test-file cypress/e2e/products.cy.js
```

And finally you can execute the suite cypress with:

```
yarn test-suite
```