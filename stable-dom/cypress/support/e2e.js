import './commands';
import '@testing-library/cypress/add-commands';
const { registerCommand } = require('cypress-wait-for-stable-dom');
registerCommand({ pollInterval: 1000, timeout: 10000 });
