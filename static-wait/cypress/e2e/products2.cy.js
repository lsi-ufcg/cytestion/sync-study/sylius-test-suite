describe('products', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  it('should see the details of a product size "S" and add to cart', function () {
    cy.visit('/en_US/products/990m-regular-fit-jeans');
    cy.get('#main-image').click();
    cy.wait(1000);
    cy.get('.lb-close').click();
    cy.get('#sylius_add_to_cart_cartItem_variant_jeans_size').select('S');
    cy.get('#sylius_add_to_cart_cartItem_quantity').type('2');
    cy.get('#sylius_add_to_cart_cartItem_quantity').click();
    cy.get('.primary').click();
    cy.wait(1000);
    cy.get('.sylius-flash-message').should('contain', 'Item has been added to cart');
  });

  it('should see the details of a product size "M" and add to cart', function () {
    cy.visit('/en_US/products/990m-regular-fit-jeans');
    cy.get('#main-image').click();
    cy.wait(1000);
    cy.get('.lb-close').click();
    cy.get('#sylius_add_to_cart_cartItem_variant_jeans_size').select('M');
    cy.get('#sylius_add_to_cart_cartItem_quantity').type('2');
    cy.get('#sylius_add_to_cart_cartItem_quantity').click();
    cy.get('.primary').click();
    cy.wait(1000);
    cy.get('.sylius-flash-message').should('contain', 'Item has been added to cart');
  });

  it('should see the details of a product size "L" and add to cart', function () {
    cy.visit('/en_US/products/990m-regular-fit-jeans');
    cy.get('#main-image').click();
    cy.wait(1000);
    cy.get('.lb-close').click();
    cy.get('#sylius_add_to_cart_cartItem_variant_jeans_size').select('L');
    cy.get('#sylius_add_to_cart_cartItem_quantity').type('2');
    cy.get('#sylius_add_to_cart_cartItem_quantity').click();
    cy.get('.primary').click();
    cy.wait(1000);
    cy.get('.sylius-flash-message').should('contain', 'Item has been added to cart');
  });

  it('should see the details of a product size "XL" and add to cart', function () {
    cy.visit('/en_US/products/990m-regular-fit-jeans');
    cy.get('#main-image').click();
    cy.wait(1000);
    cy.get('.lb-close').click();
    cy.get('#sylius_add_to_cart_cartItem_variant_jeans_size').select('XL');
    cy.get('#sylius_add_to_cart_cartItem_quantity').type('2');
    cy.get('#sylius_add_to_cart_cartItem_quantity').click();
    cy.get('.primary').click();
    cy.wait(1000);
    cy.get('.sylius-flash-message').should('contain', 'Item has been added to cart');
  });

  it('should see the details of a product size "XXL" and add to cart', function () {
    cy.visit('/en_US/products/990m-regular-fit-jeans');
    cy.get('#main-image').click();
    cy.wait(1000);
    cy.get('.lb-close').click();
    cy.get('#sylius_add_to_cart_cartItem_variant_jeans_size').select('XXL');
    cy.get('#sylius_add_to_cart_cartItem_quantity').type('2');
    cy.get('#sylius_add_to_cart_cartItem_quantity').click();
    cy.get('.primary').click();
    cy.wait(1000);
    cy.get('.sylius-flash-message').should('contain', 'Item has been added to cart');
  });
});
