describe('orders', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  it('filter on a especific product', () => {
    cy.clickInFirst('a[href="/admin/orders/"]');

    cy.get('div[class="sylius-autocomplete ui fluid search selection dropdown multiple"]').first().type('727F');

    cy.wait(1000);
    cy.get('[data-value="727F_patched_cropped_jeans"]').last().click();

    cy.get('div[class="sylius-autocomplete ui fluid search selection dropdown multiple"]').first().type('Loose');

    cy.wait(1000);
    cy.get('[data-value="Loose_white_designer_T_Shirt-variant-1"]').last().click();

    cy.get('[class="ui blue labeled icon button"]').click();

    cy.get('body').should('contain', 'There are no results to display');
  });
});
