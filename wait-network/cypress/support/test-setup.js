import { setupCypressInterception, plantWaitUntilSomeRequestOccurs } from './utils/interception';

Cypress.on('url:changed', (newUrl) => {
  plantWaitUntilSomeRequestOccurs();
  console.log('newUrl', newUrl);
});

beforeEach(() => {
  setupCypressInterception();
});
