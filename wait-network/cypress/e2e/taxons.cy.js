describe('taxons', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
    cy.clickInFirst('a[href="/admin/taxons/new"]');
  });
  it('create new taxon', () => {
    cy.get('[id="sylius_taxon_code"]').type('new-taxon');

    cy.get('div[class^="sylius-autocomplete ui fluid search selection dropdown"]').first().click('bottomRight');
    cy.waitNetworkFinished();
    cy.get('div.item[data-value="mens_t_shirts"]').click();

    cy.get('[id="sylius_taxon_translations_en_US_name"]').type('New Taxon');
    cy.get('[id="sylius_taxon_translations_en_US_slug"]').type('New Taxon slug');
    cy.get('[class="ui labeled icon primary button"]').scrollIntoView().click();
    cy.waitNetworkFinished();
    cy.get('body').should('contain', 'Taxon has been successfully created.');
  });
});
