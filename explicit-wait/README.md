# Sylius Test Suite - Cypress
A end-to-end test suite to Sylius showcase with Cypress.

You can install the sylius showcase with one docker container with this repository: [sylius-showcase-docker](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-showcase-docker).

**It's neccessary to have Nodejs and Yarn installed.**

## Execute

First install the dependencies:

```
yarn install
```

**For execute the cypress scripts** you can use the cypress interface with:

```
yarn cypress
```

You can also run an unique test file in terminal:

```
yarn test-file cypress/e2e/products.cy.js
```

And finally you can execute the suite cypress with:

```
yarn test-suite
```