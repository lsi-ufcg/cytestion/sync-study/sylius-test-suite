describe('association types', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').clear().type('sylius');
    cy.get('[id="_password"]').clear().type('sylius');
    cy.get('.primary').click();
  });

  it('defines a type of association between one product and another', () => {
    // Creating association types
    cy.clickInFirst('a[href="/admin/product-association-types/"]');

    cy.get('[id="criteria_name_value"]').type('similar_products');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="ui red labeled icon button"]').last().click();
    cy.get('[id="confirmation-button"]').first().click();
    cy.get('body').should('contain', 'Product association type has been successfully deleted.');
    
    cy.get('*[class^="ui right floated buttons"]').click();
    cy.get('[id="sylius_product_association_type_code"]').type('Clone');
    cy.get('[id="sylius_product_association_type_translations_en_US_name"]').type('Clone');
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Click in products in side menu
    cy.clickInFirst('a[href="/admin/products/"]');

    // Type in value input to search for specify product
    cy.get('[id="criteria_search_value"]').scrollIntoView().type('Knitted wool-blend green cap');

    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').scrollIntoView().click();

    // Click in edit product
    cy.get('a.ui.labeled.icon.button').contains('Edit').click();

    // Click in association option
    cy.get('a').contains('Associations').click();

    // Click in dropdown the products
    cy.get('div[class^="product-select ui fluid multiple search selection dropdown"]').last().type('Cash');

    // Wait until the element is present and visible
    cy.waitUntilExists('div.item[data-value="Cashmere_blend_violet_beanie"]');
    cy.get('div.item[data-value="Cashmere_blend_violet_beanie"]').last().click();

    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that product has been updated
    cy.get('body').should('contain', 'Product has been successfully updated.');
  });

  it('delete a type of association between one product and another', () => {
    // Click in products in side menu
    cy.clickInFirst('a[href="/admin/products/"]');

    // Type in value input to search for specify product
    cy.get('[id="criteria_search_value"]').scrollIntoView().type('Knitted wool-blend green cap');

    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').scrollIntoView().click();

    // Click in edit product
    cy.get('a.ui.labeled.icon.button').contains('Edit').click();

    // Click in association option
    cy.get('a').contains('Associations').click();

    // Wait until the delete icon is present and visible
    cy.waitUntilExists('a.ui.label[data-value="Cashmere_blend_violet_beanie"] i.delete.icon').then(() => {
      cy.get('a.ui.label[data-value="Cashmere_blend_violet_beanie"] i.delete.icon').last().click();
    });

    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that product has been updated
    cy.get('body').should('contain', 'Product has been successfully updated.');
  });
});
