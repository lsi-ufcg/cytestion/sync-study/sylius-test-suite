describe('products', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  it('Add a association product', () => {
    // Creating association types
    cy.clickInFirst('a[href="/admin/product-association-types/"]');
    cy.get('*[class^="ui right floated buttons"]').click();
    cy.get('[id="sylius_product_association_type_code"]').type('Clone2');
    cy.get('[id="sylius_product_association_type_translations_en_US_name"]').type('Clone2');
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    cy.clickInFirst('a[href="/admin/products/"]');

    cy.get('[id="criteria_search_value"]').type('000F_office_grey_jeans');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('[class="icon pencil"]').click();

    cy.get('[data-tab="associations"]').first().click();

    cy.get('div[class="product-select ui fluid multiple search selection dropdown"]').last().type('666F');

    cy.waitUntilExists('[data-value="666F_boyfriend_jeans_with_rips"]').then(() => {
      cy.get('[data-value="666F_boyfriend_jeans_with_rips"]').first().click();
    });

    cy.get('[id="sylius_save_changes_button"]').click();

    cy.get('body').should('contain', 'Product has been successfully updated.');
  });
});
