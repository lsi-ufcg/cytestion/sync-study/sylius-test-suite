describe('exchange rates', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });

  afterEach(() => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');
    // Verify if bulk delete button exists
    cy.get('div[class^="sylius-grid-nav"]')
      .children()
      .its('length')
      .should('be.gt', 0)
      .then((length) => {
        if (length > 1) {
          // Click in Select All Items of Exchange rate's list
          cy.get('th[class^="center aligned"]').click();
          // Click in the First of two Delete buttons at the page
          cy.get('button[class^="ui red labeled icon button"]')
            .first()
            .then((element) => {
              element.click();
            });
          // Click in confirmation button
          cy.get('div[id^="confirmation-button"]').click();
        }
      });
  });

  it('1. Create an exchange rate without ratio added.', () => {
    cy.addExchangeRate('USD', 'EUR', '10');
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has not been created
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('2. Create an exchange rate with equals Source and Target currency.', () => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Type ratio to 5
    cy.get('#sylius_exchange_rate_ratio').type('5');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has not been created
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('3. Create an exchange rate with negative ratio.', () => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Type ratio to -5
    cy.get('#sylius_exchange_rate_ratio').type('-5');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has not been created
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('4. Create an exchange rate with ratio equals zero.', () => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Type ratio to 0
    cy.get('#sylius_exchange_rate_ratio').type('0');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has not been created
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('5. Create a valid exchange rate.', () => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Type ratio to 10
    cy.get('#sylius_exchange_rate_ratio').type('10');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has been created
    cy.get('body').should('contain', 'Exchange rate has been successfully created.');
  });

  it('6. Add input values and click in "Cancel".', () => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Type ratio to 10
    cy.get('#sylius_exchange_rate_ratio').type('10');

    // Click on Cancel button
    cy.get('a[class^="ui button"]').scrollIntoView().click();

    // Verify some exchange rate existence
    cy.get('div[class^="sylius-grid-wrapper"]').should(($children) => {
      let content = $children[0].children[1].children[1].children[1].textContent;

      expect(content).to.be.equals('There are no results to display');
    });
  });

  it('7. Create a new exchange rate equal a existent yet.', () => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Type ratio to 10
    cy.get('#sylius_exchange_rate_ratio').type('10');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has been created
    cy.get('body').should('contain', 'Exchange rate has been successfully created.');

    // Create a other one

    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Type ratio to 10
    cy.get('#sylius_exchange_rate_ratio').type('10');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has not been created
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('8. Create a new exchange rate equal a existent yet, but with inversal order of Source and Target.', () => {
    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_sourceCurrency').select('USD');

    // Type ratio to 10
    cy.get('#sylius_exchange_rate_ratio').type('10');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has been created
    cy.get('body').should('contain', 'Exchange rate has been successfully created.');

    // Create a other one

    // Click in exchange rates in side menu
    cy.clickInFirst('a[href="/admin/exchange-rates/"]');

    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();

    // Select US Dollar Source currency
    cy.get('#sylius_exchange_rate_targetCurrency').select('USD');

    // Type ratio to 10
    cy.get('#sylius_exchange_rate_ratio').type('10');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that exchange rate has not been created
    cy.get('body').should('contain', 'This form contains errors.');
  });
});
