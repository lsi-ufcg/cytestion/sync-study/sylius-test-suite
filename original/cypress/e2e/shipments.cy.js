describe('shipments', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });

  it('test case 1', () => {
    cy.clickInFirst('a[href="/admin/shipments/"]');
    cy.get('.ui > .sylius-filters > .sylius-filters__field > .field > #criteria_state').select('Shipped');
    cy.get('*[class^="ui blue labeled icon button"]').click();

    cy.get('tbody>tr').eq(0).should('contain', '#000000012');
  });

  it('test case 2', () => {
    cy.clickInFirst('a[href="/admin/shipments/"]');
    cy.get('*[class^="ui blue labeled icon button"]').click();

    cy.get('tr[class^="item"]').should('have.length', 10);
  });

  it('test case 3', () => {
    cy.clickInFirst('a[href="/admin/shipments/"]');
    cy.get('.ui > .sylius-filters > .sylius-filters__field > .field > #criteria_state').select('Shipped');
    cy.get('*[class^="ui blue labeled icon button"]').click();

    cy.get('body').should('contain', 'Shipped');
  });

  it('test case 4', () => {
    cy.clickInFirst('a[href="/admin/shipments/"]');
    cy.get('.ui > .sylius-filters > .sylius-filters__field > .field > #criteria_state').select('Cancelled');
    cy.get('*[class^="ui blue labeled icon button"]').click();

    cy.get('body').should('contain', 'There are no results to display');
  });

  it('test case 5', () => {
    cy.clickInFirst('a[href="/admin/shipments/"]');
    cy.get('.ui > .sylius-filters > .sylius-filters__field > .field > #criteria_channel').select('Fashion Web Store');
    cy.get('*[class^="ui blue labeled icon button"]').click();

    cy.get('body').should('contain', 'Fashion Web Store');
  });

  it('test case 6', () => {
    cy.clickInFirst('a[href="/admin/shipments/"]');
    cy.get('.ui > .sylius-filters > .sylius-filters__field > .field > #criteria_state').select('Shipped');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.clickInFirst('*[class^="icon search"]');

    cy.get('tr[class^="item"]').should('not.be.empty');
  });
});
