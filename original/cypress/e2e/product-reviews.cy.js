describe('product reviews', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!
  it('changing rating of specify product review', () => {
    // Click in product reviews in side menu
    cy.clickInFirst('a[href="/admin/product-reviews/"]');
    // Type in value input to search for specify product review
    cy.get('[id="criteria_title_value"]').type('voluptatem');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in edit of the last product review
    cy.get('*[class^="ui labeled icon button "]').last().click();
    // Edit product review rating
    cy.get('[for="sylius_product_review_rating_4"]').scrollIntoView().click();
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that product review has been updated
    cy.get('body').should('contain', 'Product review has been successfully updated.');
  });
  it('Accept a review', () => {
    cy.clickInFirst('a[href="/admin/product-reviews/"]');

    cy.get('[class="ui loadable green labeled icon button"]').first().click();

    cy.get('body').should('contain', 'Review has been successfully accepted.');
  });

  it('Reject a review', () => {
    cy.clickInFirst('a[href="/admin/product-reviews/"]');

    cy.get('[class="ui loadable yellow labeled icon button"]').first().click();

    cy.get('body').should('contain', 'Review has been successfully rejected.');
  });
});
