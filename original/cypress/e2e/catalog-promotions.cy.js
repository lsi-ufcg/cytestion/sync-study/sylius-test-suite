describe('catalog promotions', () => {
  beforeEach(() => {
    const credentials = ['sylius', 'sylius'];
    cy.login(...credentials);
    cy.clickInFirst('a[href="/admin/catalog-promotions/"]');
  });
  // Remove .only and implement others test cases!
  it('shows of especify catalog promotion exposes the correct product and discount', () => {
    // Select inactive promotions in search tab
    cy.get('[id="criteria_state"]').select('Inactive');
    // Type in value input to search for specify catalog promotion
    cy.get('[id="criteria_search_value"]').type('autumn');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in shows of the remain catalog promotion
    cy.get('*[class^="ui labeled icon button "]').each(($btn, index) => {
      if (index == 1) cy.wrap($btn).click();
    });

    // Assert that shows page has important informations
    cy.get('body').should('contain', 'Knitted_wool_blend_green_cap').and('contain', 'Percentage discount').and('contain', '50%');
  });

  it('search function should show no results message when querying for active promotions if there are no active promotions', () => {
    cy.get('body').should('not.have.value', 'Active');
    cy.get('[id="criteria_search_value"]').type('autumn');
    cy.get('[id="criteria_state"]').select('Active');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('body').should('contain', 'Info').and('contain', 'There are no results to display');
  });

  it('should create a new promotion succesfully if given correct input (unique name and code)', () => {
    cy.get('*[class^="icon plus"]').click();
    cy.get('[id="sylius_catalog_promotion_code"]').type('test_code');
    cy.get('[id="sylius_catalog_promotion_name"]').type('test_name');
    cy.get('*[class^="ui labeled icon primary button"]').click();
    cy.get('body').should('contain', 'Success').and('contain', 'Catalog promotion has been successfully created.');
  });

  it("should be able to edit a promotion's name succesfully", () => {
    cy.get('[id="criteria_search_value"]').type('autumn');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="icon pencil"]').click();
    cy.get('[id="sylius_catalog_promotion_name"]').clear().type('new_name');
    cy.get('*[class^="ui labeled icon primary button"]').click();
    cy.get('body').should('contain', 'Success').and('contain', 'Catalog promotion has been successfully updated.');
  });

  it("should be able to edit a promotion's start and end date succesfully", () => {
    cy.get('[id="criteria_search_value"]').type('autumn');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="icon pencil"]').click();
    cy.get('[id="sylius_catalog_promotion_startDate_date"]').clear().type('2023-01-01');
    cy.get('[id="sylius_catalog_promotion_endDate_date"]').clear().type('2023-12-31');
    cy.get('*[class^="ui labeled icon primary button"]').click();
    cy.get('body').should('contain', 'Success').and('contain', 'Catalog promotion has been successfully updated.');
    cy.clickInFirst('a[href="/admin/catalog-promotions/"]');
    cy.get('[id="criteria_search_value"]').type('autumn');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('body').should('contain', '2023-01-01').and('contain', '2023-12-31');
  });

  it('should be able to request a promotion deletion succesfully', () => {
    cy.get('[id="criteria_search_value"]').type('summer');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="icon trash"]').click();
    cy.get('[id="confirmation-button"]').click();
    cy.get('body').should('contain', 'Success').and('contain', 'Removing of catalog promotion has been requested.');
  });

  it('should be possible to disable a promotion succesfully', () => {
    cy.get('[id="criteria_search_value"]').type('spring');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="icon pencil"]').click();
    cy.get('*[class^="ui toggle checkbox"]').first().click();
    cy.get('*[class^="ui labeled icon primary button"]').click();
    cy.get('body').should('contain', 'Success').and('contain', 'Catalog promotion has been successfully updated.');
    cy.clickInFirst('a[href="/admin/catalog-promotions/"]');
    cy.get('[id="criteria_search_value"]').type('spring');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('body').should('contain', 'Disabled');
  });

  it("should be able to edit a promotion's portuguese label and description succesfully", () => {
    cy.get('[id="criteria_search_value"]').type('autumn');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="icon pencil"]').click();
    cy.get('[data-locale="pt_PT"]', { timeout: 10000 }).click();
    cy.get('[id="sylius_catalog_promotion_translations_pt_PT_label"]').clear().type('label_teste_em_portugues');
    cy.get('[id="sylius_catalog_promotion_translations_pt_PT_description"]').clear().type('descricao_teste_em_portugues');
    cy.get('*[class^="ui labeled icon primary button"]').click();
    cy.get('body').should('contain', 'Success').and('contain', 'Catalog promotion has been successfully updated.');
  });

  it('should be able to edit a promotion with a percentage discount succesfully', () => {
    cy.get('[id="criteria_search_value"]').type('autumn');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="icon pencil"]').click();
    cy.get('[id="sylius_catalog_promotion_actions_0_configuration_amount"]').clear().type('10');
    cy.get('*[class^="ui labeled icon primary button"]').click();
    cy.get('body').should('contain', 'Success').and('contain', 'Catalog promotion has been successfully updated.');
    cy.clickInFirst('a[href="/admin/catalog-promotions/"]');
    cy.get('[id="criteria_search_value"]').type('autumn');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="ui labeled icon button "]').each(($btn, index) => {
      if (index == 1) cy.wrap($btn).click();
    });
    cy.get('body').should('contain', 'Percentage discount').and('contain', '10%');
  });
});
