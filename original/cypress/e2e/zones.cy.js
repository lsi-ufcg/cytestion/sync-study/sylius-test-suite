describe('zones', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!
  it('change scope of rest of the world zone', () => {
    // Click in zones in side menu
    cy.clickInFirst('a[href="/admin/zones/"]');
    // Type in value input to search for specify zone
    cy.get('[id="criteria_name_value"]').type('World');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in edit of the remain zone
    cy.get('*[class^="ui labeled icon button "]').last().click();
    // Edit zone scope to shipping
    cy.get('#sylius_zone_scope').select('shipping');
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that zone scope has been updated
    cy.get('body').should('contain', 'Zone has been successfully updated.');
  });

  it('create zone country', () => {
    cy.clickInFirst('a[href="/admin/zones/"]');
    // Click in filter blue button
    cy.get('*[class^="ui right floated buttons"]').click();
    cy.clickInFirst('a[href="/admin/zones/country/new"]');
    cy.get('[id="sylius_zone_code"]').type('19');
    cy.get('[id="sylius_zone_name"]').type('america');
    cy.get('#sylius_zone_scope').select('shipping');
    cy.get('a.ui.labeled.icon.button[data-form-collection="add"]').click();
    cy.get('select#sylius_zone_members_0_code').select('MX');
    cy.get('button.ui.labeled.icon.primary.button').click();

    cy.get('body').should('contain', 'Zone has been successfully created.');
  });
  it('delete zone', () => {
    cy.clickInFirst('a[href="/admin/zones/"]');

    cy.get('table.ui.sortable.stackable.very.basic.celled.table')
      .find('tr.item') // Seleciona todos os elementos <tr> com a classe "item"
      .eq(2) // Seleciona o terceiro elemento <tr>
      .find('button.ui.red.labeled.icon.button') // Localiza o botão "Delete" dentro do elemento <tr>
      .click(); // Clica no botão "Delete"
    cy.get('div.ui.green.ok.inverted.button#confirmation-button').click();

    cy.get('body').should('contain', 'Zone has been successfully deleted.');
  });

  it('create zone outher', () => {
    cy.clickInFirst('a[href="/admin/zones/"]');

    cy.get('*[class^="ui right floated buttons"]').click();
    cy.clickInFirst('a[href="/admin/zones/zone/new"]');
    cy.get('[id="sylius_zone_code"]').type('19');
    cy.get('[id="sylius_zone_name"]').type('america');
    cy.get('#sylius_zone_scope').select('shipping');
    cy.get('a.ui.labeled.icon.button[data-form-collection="add"]').click();
    cy.get('select#sylius_zone_members_0_code').select('WORLD');

    cy.get('button.ui.labeled.icon.primary.button').click();

    cy.get('body').should('contain', 'Zone has been successfully created.');

    cy.clickInFirst('a[href="/admin/zones/"]');

    cy.get('table.ui.sortable.stackable.very.basic.celled.table')
      .find('tr.item') // Seleciona todos os elementos <tr> com a classe "item"
      .eq(2) // Seleciona o terceiro elemento <tr>
      .find('button.ui.red.labeled.icon.button') // Localiza o botão "Delete" dentro do elemento <tr>
      .click(); // Clica no botão "Delete"
    cy.get('div.ui.green.ok.inverted.button#confirmation-button').click();
  });

  it('Testando paginação da pagina de zonas', () => {
    cy.clickInFirst('a[href="/admin/zones/"]');
    const zones = [
      ['my_simple', 'my simple'],
      ['my_simple_region', 'my simple region'],
      ['simple_region', 'simple region'],
      ['aaa', 'bbb'],
      ['bbb', 'bbb'],
      ['ccc', 'ccc'],
      ['ddd', 'ddd'],
      ['eee', 'eee'],
      ['fff', 'fff'],
      ['ggg', 'ggg'],
    ];

    zones.forEach((e) => {
      cy.clickInFirst('a[href="/admin/zones/"]');
      cy.get('.right > .ui').click();
      cy.get('.middle > .ui > .ui > .menu > .item:nth-child(3)').click();
      cy.get('#sylius_zone_code').type(e[0]);
      cy.get('#sylius_zone_name').type(e[1]);
      cy.get('#sylius_zone_members > .ui').click();
      cy.get('div > .ui > .inline > .required > #sylius_zone_members_0_code').select('FR');
      cy.get('.buttons > .labeled').click();
    });

    cy.clickInFirst('a[href="/admin/zones/"]');
    cy.get('.sylius-grid-wrapper > :nth-child(3) > :nth-child(3)').click();
    cy.get('.sylius-grid-wrapper > :nth-child(3) > .active').should('have.class', 'active item');
  });

  it('Adicionando novo pais na zona', () => {
    cy.clickInFirst('a[href="/admin/zones/"]');
    cy.get('[id="criteria_name_value"]').type('my_simple');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="ui labeled icon button "]').last().click();
    cy.get('#sylius_zone_members > .ui').click();
    cy.get('#sylius_zone_members_1_code').select('PT');
    cy.get('.buttons > .labeled').click();
    cy.get('body').should('contain', 'Zone has been successfully updated.');
  });

  it('removendo pais de uma zona', () => {
    cy.clickInFirst('a[href="/admin/zones/"]');
    cy.get('[id="criteria_name_value"]').type('my_simple');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.get('*[class^="ui labeled icon button "]').last().click();
    cy.get('[data-form-collection-index="1"] > .red').click();
    cy.get('.buttons > .labeled').click();
    cy.get('body').should('contain', 'Zone has been successfully updated.');
  });

  it('Testando filto da pagina de zonas', () => {
    cy.clickInFirst('a[href="/admin/zones/"]');
    cy.get('[id="criteria_name_value"]').type('my');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.contains('my_simple');
    cy.contains('my_simple_region');
    cy.get('#criteria_code_value').type('region');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.contains('my_simple_region');

    cy.clickInFirst('a[href="/admin/zones/"]');
    cy.get('[id="criteria_name_value"]').type('simple');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.contains('my_simple');
    cy.contains('my_simple_region');
    cy.contains('simple_region');
    cy.get('#criteria_code_value').type('region');
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.contains('my_simple_region');
    cy.contains('simple_region');
  });
});
