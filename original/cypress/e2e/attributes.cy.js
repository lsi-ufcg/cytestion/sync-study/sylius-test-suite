describe('attributes', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!
  it('testing edit attribute position', () => {
    // Click in products in side menu
    cy.clickInFirst('a[href="/admin/product-attributes/"]');
    // Type in value input to search for specify attribute
    cy.get('[id="criteria_code_value"]').type('dress_collection');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in edit of the remain attribute
    cy.clickInFirst('*[class^="ui labeled icon button "]');
    // Edit attribute position
    cy.get('[id="sylius_product_attribute_position"]').clear().type('10');
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that attribute has been updated
    cy.get('body').should('contain', 'Product attribute has been successfully updated.');
  });

  it('test case 2 - testing attribute search t_shirt_collection', () => {
    cy.clickInFirst('a[href="/admin/product-attributes/"]');
    cy.wait(500);
    cy.get('#criteria_code_value').type('t_shirt_collection');
    cy.wait(500);
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.wait(500);
    cy.get('body').each(($el) => {
      expect($el.text()).to.include('T-shirt collection');
    });
  });

  it('test case 3 - Test checks if it gives an error when entering a wrong attribute code', () => {
    cy.clickInFirst('a[href="/admin/product-attributes/"]');
    cy.wait(500);
    cy.get('.ui.labeled.icon.top.right.floating.dropdown.button.primary.link').click();
    cy.wait(500);
    cy.get('#text.item').click();
    cy.get('#sylius_product_attribute_code').type('Criando teste');
    cy.wait(500);
    cy.get('#sylius_product_attribute_position').clear().type('0');
    cy.wait(500);
    cy.get('#sylius_product_attribute_translations_en_US_name').scrollIntoView().clear().type('Teste');
    cy.wait(500);
    cy.get('button.ui.labeled.icon.primary.button').scrollIntoView().click();
    cy.wait(500);
    cy.get('div.ui.red.pointing.label.sylius-validation-error').should('be.visible');
  });
});
