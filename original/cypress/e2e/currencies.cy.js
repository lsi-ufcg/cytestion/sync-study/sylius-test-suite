describe('currencies', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!
  it('validate that cannot create the same currency twice', () => {
    // Click in currencies in side menu
    cy.clickInFirst('a[href="/admin/currencies/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Select Euro currency
    cy.get('#sylius_currency_code').select('EUR');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();
    // Assert that cannot create the same currency twice
    cy.get('body').should('contain', 'This form contains errors.').and('contain', 'Currency code must be unique.');
  });
  it('verifica se o filtro "contains" com a palavra "AUD" encontra o elemento', () => {
    // Click in currencies in side menu
    cy.clickInFirst('a[href="/admin/currencies/"]');
    // Select "contains"
    cy.get('select[name="criteria[code][type]"]').select('Contains');
    // Digita no campo de busca "AUD"
    cy.get('#criteria_code_value').type('AUD');
    // Clicla em "Filtrar"
    cy.get('button.ui.blue.labeled.icon.button[type="submit"]').click();
    // Mostra que existe um "AUD"
    cy.get('.ui.sortable.stackable.very.basic.celled.table').should('contain', 'AUD');
  });

  it('verifica se o filtro "not contains" com a palvra "AUD" não mostra o campo correspondente', () => {
    // Click in currencies in side menu
    cy.clickInFirst('a[href="/admin/currencies/"]');
    // Select "contains"
    cy.get('select[name="criteria[code][type]"]').select('Not contains');
    // Digita no campo de busca "AUD"
    cy.get('#criteria_code_value').type('AUD');
    // Clicla em "Filtrar"
    cy.get('button.ui.blue.labeled.icon.button[type="submit"]').click();
    // Mostra que não existe um "AUD"
    cy.get('.ui.sortable.stackable.very.basic.celled.table').should('not.contain', 'AUD');
  });

  it('verifica se ao clicar no dropbox de filtro esconde o filtro e se ele retorna ao clicar novamente', () => {
    cy.clickInFirst('a[href="/admin/currencies/"]');

    cy.get('.admin-layout > .admin-layout__body > .admin-layout__content > .ui > .title').click();

    cy.get('[class^="ui styled fluid accordion"]').should('not.contain', 'title active');

    cy.wait(2000);

    cy.get('.admin-layout > .admin-layout__body > .admin-layout__content > .ui > .title').click();

    cy.get('[class^="ui styled fluid accordion"]').should('not.contain', 'title');
  });

  it('verifica se salvar as informações na parte de edição cria uma mensagem de sucesso', () => {
    cy.clickInFirst('a[href="/admin/currencies/"]');

    cy.get('tbody > .item:nth-child(1) > td > .ui > .ui').click();

    cy.get('.admin-layout__content > .ui > .ui > .ui > #sylius_save_changes_button').click();

    cy.get('.ui.icon.positive.message.sylius-flash-message').should('contain', 'Currency has been successfully updated.');
  });

  it('verifica se da para fechar a mensagem de sucesso', () => {
    cy.clickInFirst('a[href="/admin/currencies/"]');

    cy.get('tbody > .item:nth-child(1) > td > .ui > .ui').click();

    cy.get('.admin-layout__content > .ui > .ui > .ui > #sylius_save_changes_button').click();

    cy.get('.ui.icon.positive.message.sylius-flash-message').should('contain', 'Currency has been successfully updated.');

    cy.get('.admin-layout > .admin-layout__body > .admin-layout__content > .ui > .close').click();

    cy.get('.admin-layout__content >').should('not.contain', '.ui.icon.positive.message.sylius-flash-message');
  });

  it('validate that cannot create the same currency twice', () => {
    // Click in currencies in side menu
    cy.clickInFirst('a[href="/admin/currencies/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Select Euro currency
    cy.get('#sylius_currency_code').select('EUR');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();
    // Assert that cannot create the same currency twice
    cy.get('body').should('contain', 'This form contains errors.').and('contain', 'Currency code must be unique.');
  });

  it('verifica se o filtro "contains" com a palavra "AUD" encontra o elemento', () => {
    // Click in currencies in side menu
    cy.clickInFirst('a[href="/admin/currencies/"]');
    // Select "contains"
    cy.get('select[name="criteria[code][type]"]').select('Contains');
    // Digita no campo de busca "AUD"
    cy.get('#criteria_code_value').type('AUD');
    // Clicla em "Filtrar"
    cy.get('button.ui.blue.labeled.icon.button[type="submit"]').click();
    // Mostra que existe um "AUD"
    cy.get('.ui.sortable.stackable.very.basic.celled.table').should('contain', 'AUD');
  });

  it('verifica se o filtro "not contains" com a palvra "AUD" não mostra o campo correspondente', () => {
    // Click in currencies in side menu
    cy.clickInFirst('a[href="/admin/currencies/"]');
    // Select "contains"
    cy.get('select[name="criteria[code][type]"]').select('Not contains');
    // Digita no campo de busca "AUD"
    cy.get('#criteria_code_value').type('AUD');
    // Clicla em "Filtrar"
    cy.get('button.ui.blue.labeled.icon.button[type="submit"]').click();
    // Mostra que não existe um "AUD"
    cy.get('.ui.sortable.stackable.very.basic.celled.table').should('not.contain', 'AUD');
  });

  it('verifica se salvar as informações na parte de edição cria uma mensagem de sucesso', () => {
    cy.clickInFirst('a[href="/admin/currencies/"]');

    cy.get('tbody > .item:nth-child(1) > td > .ui > .ui').click();

    cy.get('.admin-layout__content > .ui > .ui > .ui > #sylius_save_changes_button').click();

    cy.get('.ui.icon.positive.message.sylius-flash-message').should('contain', 'Currency has been successfully updated.');
  });
});
