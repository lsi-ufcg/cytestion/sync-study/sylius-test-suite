describe('tax rates', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });

  it('try to create a tax rate with error', () => {
    // Click in tax rates in side menu
    cy.clickInFirst('a[href="/admin/tax-rates/"]');
    // clica no botão de criar
    cy.get('.admin-layout__content > div > .middle > div > a').click();
    // adiciona código
    cy.get('input[name="sylius_tax_rate[code]"]').type('teste');
    // adicionar nome
    cy.get('input[name="sylius_tax_rate[name]"]').type('teste');
    // clica em create
    cy.get('.admin-layout__content form > .ui.buttons > button').click();

    // verifica se teve alerta de erro
    cy.get('body').should('contain', 'Please select tax zone.');
  });

  it('show that exists tax rates in car', () => {
    // vai para uma página de produto
    cy.visit('/en_US/products/knitted-burgundy-winter-cap');
    // muda o numero ce produtos
    cy.get('#sylius-product-selecting-variant > form > div > input').clear().type(2);
    // adiciona produto no carrinho
    cy.get('#sylius-product-selecting-variant > form > button').click();
    // vai para o carrinho
    cy.visit('/en_US/cart/');

    // verifica se na área de preço tem uma que informa sobre as taxas
    cy.get('body').should('contain', 'Taxes total:');
  });
});
