describe('tax categories', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!

  it('create a new tax category', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('44');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('44');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('4444');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();
    // Assert that tax category has been created.
    cy.get('body').should('contain', 'Tax category has been successfully created.');
  });

  it('create a new tax category that already exists', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('44');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('44(2)');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('4444(2)');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();
    // Assert that tax category has not been created.
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('create a new tax category with invalid name', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('60');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('A');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('60');

    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();
    // Assert that tax category has not been created.
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('create tax categories that will be used', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('45');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('45');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('4545');
    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('50');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('50');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('5050');
    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('60');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('60');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('6060');
    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('70');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('70');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('7070');
    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('80');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('80');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('8080');
    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on create button
    cy.get('*[class^="ui labeled icon button  primary "]').click();
    // Type category code
    cy.get('[id="sylius_tax_category_code"]').type('90');
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('90');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('9090');
    // Click on create button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();
  });

  it('using the filter', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');

    // Type category name
    cy.get('[id="criteria_search_value"]').type('4');

    // Click on filter button
    cy.get('*[class^="ui blue labeled icon button"]').click();

    // Compare the number of tax categories are showed after filtering
    cy.get('table.ui.sortable.stackable.very.basic.celled.table tbody').find('tr.item').its('length').should('be.equal', 2);
  });

  it('update a tax category', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on update button
    cy.get('*[class^="ui labeled icon button "]').eq(1).click();
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('new name');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('new description');

    // Click on update button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that tax category has been updated.
    cy.get('body').should('contain', 'Tax category has been successfully updated.');
  });

  it('update a new tax category with invalid name', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');
    // Click on update button
    cy.get('*[class^="ui labeled icon button "]').eq(1).click();
    // Clear category name
    cy.get('[id="sylius_tax_category_name"]').clear();
    // Type category name
    cy.get('[id="sylius_tax_category_name"]').type('A');
    // Type category description
    cy.get('[id="sylius_tax_category_description"]').type('4444');

    // Click on update button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();
    // Assert that tax category has not been created.
    cy.get('body').should('contain', 'This form contains errors.');
  });

  it('delete a tax category', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');

    // Click on delete button
    cy.get('*[class^="ui red labeled icon button"]').eq(1).click();

    // Click on confirm button
    cy.get('*[class^="ui green ok inverted button"]').click();

    // Assert that tax category has been deleted.
    cy.get('body').should('contain', 'Tax category has been successfully deleted.');
  });

  it('delete one tax category by the checkbox', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');

    // Click on the checkbox
    cy.get('*[class^="bulk-select-checkbox"]').eq(0).click();

    // Click on delete button
    cy.get('*[class^="ui red labeled icon button"]').eq(1).click();

    // Click on confirm button
    cy.get('*[class^="ui green ok inverted button"]').click();

    // Assert that tax category has been deleted.
    cy.get('body').should('contain', 'Tax category has been successfully deleted.');
  });

  it('delete two tax categories by the checkbox', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');

    // Click on the first checkbox
    cy.get('*[class^="bulk-select-checkbox"]').eq(0).click();

    // Click on the second checkbox
    cy.get('*[class^="bulk-select-checkbox"]').eq(1).click();

    // Click on delete button
    cy.get('*[class^="ui red labeled icon button"]').eq(0).click();

    // Click on confirm button
    cy.get('*[class^="ui green ok inverted button"]').click();

    // Assert that two tax categories have been deleted.
    cy.get('body').should('contain', 'Tax_categories have been successfully deleted.');
  });

  it('delete all tax categories by the checkbox', () => {
    // Click in tax categories in side menu
    cy.clickInFirst('a[href="/admin/tax-categories/"]');

    // Click on the all checkbox
    cy.get('*[class^="center aligned"]').eq(0).click();

    // Click on delete button
    cy.get('*[class^="ui red labeled icon button"]').eq(0).click();

    // Click on confirm button
    cy.get('*[class^="ui green ok inverted button"]').click();

    // Assert that all tax categories have been deleted.
    cy.get('body').should('contain', 'Tax_categories have been successfully deleted.');
  });
});
