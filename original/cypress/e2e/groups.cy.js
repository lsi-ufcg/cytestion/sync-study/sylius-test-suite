describe('groups', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!
  it('update the name of Wholesale group', () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    cy.wait(500);
    // Type in value input to search for specify group
    cy.get('[id="criteria_search_value"]').type('wholesale');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in edit of the last group
    cy.get('*[class^="ui labeled icon button "]').last().click();
    cy.wait(500);
    // Edit group name
    cy.get('[id="sylius_customer_group_name"]').scrollIntoView().clear().type('Wholesale 100');
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that group has been updated
    cy.get('body').should('contain', 'Customer group has been successfully updated.');
  });
  //creates a new group with name and code "auxiliar"
  it('Creates new group auxiliar', async () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    cy.wait(500);
    //clicks on the create button
    cy.get('*[class^="icon plus"]').click();
    cy.wait(500);
    //type code and name
    cy.get('[id="sylius_customer_group_code"]').type('auxiliar');
    cy.get('[id="sylius_customer_group_name"]').type('auxiliar');

    //clicks on create icon
    cy.get('*[class^="plus icon"]').click();
    cy.wait(500);
    // Assert that group has been created
    cy.get('body').should('contain', 'Customer group has been successfully created.');
  });

  // Tries to create a group with same code of an existing one
  it('Does not create a retail group', async () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    cy.wait(500);
    //clicks on the create button
    cy.get('*[class^="icon plus"]').click();

    //type code and name
    cy.get('[id="sylius_customer_group_code"]').type('retail');
    cy.get('[id="sylius_customer_group_name"]').type('retail');

    //clicks on create icon
    cy.get('*[class^="plus icon"]').click();
    cy.wait(500);
    // Assert that group has been created
    cy.get('body').should('contain', 'Customer group code has to be unique.');
  });

  // Does not create a group with name length < 1
  it('Does not create a group with name length < 1', async () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    cy.wait(500);
    //clicks on the create button
    cy.get('*[class^="icon plus"]').click();

    //type code and name
    cy.get('[id="sylius_customer_group_code"]').type('smallgroup');
    cy.get('[id="sylius_customer_group_name"]').type('a');

    //clicks on create icon
    cy.get('*[class^="plus icon"]').click();
    cy.wait(500);
    // Assert that group was not created
    cy.get('body').should('contain', 'Customer group name must be at least 2 characters long.');
  });

  //filters a group that starts with letter r
  //should show retail
  it('Filters groups that starts with r', async () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    cy.wait(500);
    // select starts with and type a
    cy.get('[id="criteria_search_type"]').select('starts_with');
    cy.get('[id="criteria_search_value"]').type('r');

    //clicks on the filter button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    cy.wait(500);
    //assert that retail is shown and wholesale is not
    const body = cy.get('body');
    body.should('contain', 'Retail');
    body.should('not.contain', 'Wholesale');
  });

  //filters a group that doesnot contains letter a
  //should not show retail or wholesale
  it('Filters groups that does not contains a', async () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    cy.wait(500);
    // select not contains and type a
    cy.get('[id="criteria_search_type"]').select('not_contains');
    cy.get('[id="criteria_search_value"]').type('a');

    //clicks on the filter button
    cy.get('*[class^="ui blue labeled icon button"]').click();

    cy.wait(500);
    //assert that retail is shown and wholesale is not

    const body = cy.get('body');
    body.should('not.contain', 'Retail').and('not.contain', 'Wholesale');
  });

  // Delete a group in use
  it('test case 7', () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    // Search for specify group
    cy.get('[id="criteria_search_value"]').type('wholesale');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click delete group
    cy.get('*[class^="ui red labeled icon button"]').last().click();
    // Click in confirmation button
    cy.get('[id="confirmation-button"]').click();

    // Assert that the group has not been deleted
    cy.get('body').should('contain', 'Cannot delete, the Customer group is in use.');
  });

  // Update the name of Wholesale group to W (name length < 2)
  it('test case 8', () => {
    // Click in groups in side menu
    cy.clickInFirst('a[href="/admin/customer-groups/"]');
    // Search for specify group
    cy.get('[id="criteria_search_value"]').type('wholesale');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in edit of the last group
    cy.get('*[class^="ui labeled icon button "]').last().click();
    // Edit group name
    cy.get('[id="sylius_customer_group_name"]').scrollIntoView().clear().type('W');
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that the group has not been updated
    cy.get('body').should('contain', 'This form contains errors.');
  });
  // Update a group on the customer
  it('test case 10', () => {
    // Click in customers in side menu
    cy.clickInFirst('a[href="/admin/customers/"]');
    // Click on the first edit you find
    cy.get('a:contains(Edit)').first().scrollIntoView().click();
    // Select a specific group
    cy.get('[id="sylius_customer_group"]').select('retail');
    // Click on Save changes button
    cy.get('*[class^="ui labeled icon primary button"]').scrollIntoView().click();

    // Assert that customer has been updated
    cy.get('body').should('contain', 'Customer has been successfully updated.');
  });

  // Implement the remaining test cases in a similar manner
});
