describe('orders', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!
  it('details of an order shows correct values', () => {
    // Click in orders in side menu
    cy.clickInFirst('a[href="/admin/orders/"]');
    // Type in value input to search for specify order
    cy.get('[id="criteria_total_greaterThan"]').type('500');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in details of the remain order
    cy.clickInFirst('*[class^="ui labeled icon button "]');

    // Assert that details page shows important informations
    cy.get('body').should('contain', 'Order total').and('contain', 'Payments').and('contain', 'Shipments').and('contain', 'Customer since').and('contain', 'Shipping');
  });

  it('verify if at click in the dropbox of filter that hides and shows again', () => {
    cy.clickInFirst('a[href="/admin/orders/"]');

    cy.get('.admin-layout > .admin-layout__body > .admin-layout__content > .ui > .title').click();

    cy.get('[class^="ui styled fluid accordion"]').should('not.contain', 'title active');

    cy.wait(2000);

    cy.get('.admin-layout > .admin-layout__body > .admin-layout__content > .ui > .title').click();

    cy.get('[class^="ui styled fluid accordion"]').should('not.contain', 'title');
  });

  it('clear filters', () => {
    cy.clickInFirst('a[href="/admin/orders/"]');

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_customer_value').type('Matilde');

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_number_value').type('5');

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_total_greaterThan').type('7');

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_total_lessThan').type('20');

    cy.get('.admin-layout__content > .ui > .content > .ui > .ui:nth-child(3)').click();

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_customer_value').should('have.value', '');
    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_number_value').should('have.value', '');
    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_total_greaterThan').should('have.value', '');
    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_total_lessThan').should('have.value', '');
  });

  it('filter on a especific product', () => {
    cy.clickInFirst('a[href="/admin/orders/"]');

    cy.get('[class="sylius-autocomplete ui fluid search selection dropdown multiple"]').first().click();
    cy.get('[data-value="727F_patched_cropped_jeans"]').click();

    cy.get('[class="sylius-autocomplete ui fluid search selection dropdown multiple"]').first().click();
    cy.get('[data-value="Loose_white_designer_T_Shirt-variant-1"]').click();

    cy.get('[class="ui blue labeled icon button"]').click();

    cy.get('body').should('contain', 'There are no results to display');
  });
});
