describe('cart promotions', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove only and implement others test cases!
  it('increase percentage of christmas promotion', () => {
    // Click in cart promotions in side menu
    cy.clickInFirst('a[href="/admin/promotions/"]');
    // Type in value input to search for specify cart promotion
    cy.get('[id="criteria_search_value"]').type('christmas');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in edit of the remain cart promotion
    cy.get('*[class^="ui labeled icon button "]').last().click();
    // Edit cart promotion percentage inside configuration
    cy.get('[id="sylius_promotion_actions_0_configuration_percentage"]').scrollIntoView().clear().type('10');
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that cart promotion name has been updated
    cy.get('body').should('contain', 'Promotion has been successfully updated.');
  });

  it('Edit promotion New Year', () => {
    cy.clickInFirst('a[href="/admin/promotions/"]');

    cy.get('.item:nth-child(1) > td > .ui > .ui > .icon').click();

    cy.get('.column > .ui > .column > .field > #sylius_promotion_usageLimit').click();

    cy.get('.column > .ui > .column > .field > #sylius_promotion_usageLimit').type('0');

    cy.get('.admin-layout__content > .ui > .ui > .ui > #sylius_save_changes_button').click();

    cy.get('body').should('contain', 'Promotion has been successfully updated.');
  });

  it('Checks number of promotions with filters', () => {
    cy.clickInFirst('a[href="/admin/promotions/"]');

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_search_value').click();

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_search_value').type('a');

    cy.get('.ui > .content > .ui > .ui:nth-child(2) > .icon').click();

    cy.get('tbody').children().should('have.length', 2);
  });

  it('Checks alteration of promotions', () => {
    cy.clickInFirst('a[href="/admin/promotions/"]');

    cy.get('.admin-layout__content > .ui > .middle > .ui > .ui').click();

    cy.visit('/admin/promotions/new');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').click();

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').type('Simple');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_name').type('Example');

    cy.get('.ui > .ui > .ui > .ui > .plus').click();

    cy.visit('/admin/promotions/');

    cy.get('tbody').children().should('have.length', 3);
  });

  it('Delete a cart promotion', () => {
    cy.visit('/admin/promotions/');

    cy.get('.item:nth-child(2) > td > .ui > form > .ui').click();

    cy.get('.dimmable > .ui > #confirmation-modal > .actions > #confirmation-button').click();

    cy.get('body').should('contain', 'Promotion has been successfully deleted.');
  });

  it('Creates new promotion', () => {
    cy.clickInFirst('a[href="/admin/promotions/"]');

    cy.get('.admin-layout__content > .ui > .middle > .ui > .ui').click();

    cy.visit('/admin/promotions/new');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').click();

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').type('12345');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_name').click();

    cy.get('.column > .ui > .two > .required > #sylius_promotion_name').type('Testing');

    cy.get('.column > .ui > .column > .field > #sylius_promotion_priority').click();

    cy.get('.column > .ui > .column > .field > #sylius_promotion_priority').type('3');

    cy.get('.column > .ui > .column > .field > #sylius_promotion_usageLimit').click();

    cy.get('.column > .ui > .column > .field > #sylius_promotion_usageLimit').type('5');

    cy.get('.column > .two > .field > #sylius_promotion_startsAt > #sylius_promotion_startsAt_date').click();

    cy.get('.column > .two > .field > #sylius_promotion_startsAt > #sylius_promotion_startsAt_date').click();

    cy.get('.column > .two > .field > #sylius_promotion_startsAt > #sylius_promotion_startsAt_date').click();

    cy.get('.column > .two > .field > #sylius_promotion_startsAt > #sylius_promotion_startsAt_date').type('2023-05-31');

    cy.get('.column > .two > .field > #sylius_promotion_endsAt > #sylius_promotion_endsAt_date').click();

    cy.get('.column > .two > .field > #sylius_promotion_endsAt > #sylius_promotion_endsAt_date').type('2023-06-02');

    cy.get('.column > .two > .field > #sylius_promotion_startsAt > #sylius_promotion_startsAt_time').click();

    cy.get('.column > .two > .field > #sylius_promotion_startsAt > #sylius_promotion_startsAt_time').type('01:12');

    cy.get('.column > .two > .field > #sylius_promotion_endsAt > #sylius_promotion_endsAt_time').click();

    cy.get('.column > .two > .field > #sylius_promotion_endsAt > #sylius_promotion_endsAt_time').type('11:14');

    cy.get('.admin-layout__content > .ui > .ui > .ui:nth-child(5) > .ui:nth-child(1)').click();

    cy.get('body').should('contain', 'Promotion has been successfully created.');
  });

  it('Checks filter new created promotion', () => {
    cy.clickInFirst('a[href="/admin/promotions/"]');

    cy.get('.admin-layout__content > .ui > .middle > .ui > .ui').click();

    cy.visit('/admin/promotions/new');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').click();

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').type('Filter');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_name').type('This');

    cy.get('.admin-layout__content > .ui > .ui > .ui:nth-child(5) > .ui:nth-child(1)').click();

    cy.visit('/admin/promotions/');

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_search_value').click();

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_search_value').type('Filter');

    cy.get('.admin-layout__content > .ui > .content > .ui > .ui:nth-child(2)').click();

    cy.get('tbody > tr > td:nth-child(3)').should('contain', 'Filter');
  });

  it('Create cupom and filter it', () => {
    cy.clickInFirst('a[href="/admin/promotions/"]');

    cy.get('.admin-layout__content > .ui > .middle > .ui > .ui').click();

    cy.visit('/admin/promotions/new');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').click();

    cy.get('.column > .ui > .two > .required > #sylius_promotion_code').type('Coupon_Filter');

    cy.get('.column > .ui > .two > .required > #sylius_promotion_name').click();

    cy.get('.column > .ui > .two > .required > #sylius_promotion_name').type('Cupom');

    cy.get('.column > .ui > .column > .field > #sylius_promotion_usageLimit').click();

    cy.get('.column > .ui > .column > .field > #sylius_promotion_usageLimit').type('100');

    cy.get('.ui > .column > .field:nth-child(2) > .ui > label').click();

    cy.get('.ui > .column > .field > .ui > #sylius_promotion_couponBased').check('1');

    cy.get('.ui > .column > .required > .toggle > .required').click();

    cy.get('.ui > .column > .required > .ui > #sylius_promotion_exclusive').check('1');

    cy.get('#sylius_promotion_channels > .required > .field > .ui > label').click();

    cy.get('#sylius_promotion_channels > .required > .field > .ui > #sylius_promotion_channels_0').check('FASHION_WEB');

    cy.get('.ui > #actions > .required > #sylius_promotion_actions > .ui').click();

    cy.get('.required > #sylius_promotion_actions_0_configuration_FASHION_WEB > .required > .ui > #sylius_promotion_actions_0_configuration_FASHION_WEB_amount').click();

    cy.get('.required > #sylius_promotion_actions_0_configuration_FASHION_WEB > .required > .ui > #sylius_promotion_actions_0_configuration_FASHION_WEB_amount').type('10');

    cy.get('.admin-layout__content > .ui > .ui > .ui:nth-child(5) > .ui:nth-child(1)').click();

    cy.wait(1000);

    cy.get('.ui > .six > .ui > .ui > .icon').click();

    cy.get('.admin-layout__content > .ui > .middle > .ui > .ui:nth-child(2)').click();

    cy.get('.ui > .ui > .ui > .required > #sylius_promotion_coupon_code').click();

    cy.get('.ui > .ui > .ui > .required > #sylius_promotion_coupon_code').type('FILTER_CUPOM');

    cy.get('.ui > .ui > .two > .field > #sylius_promotion_coupon_usageLimit').click();

    cy.get('.ui > .ui > .two > .field > #sylius_promotion_coupon_usageLimit').type('10');

    cy.get('.ui > .ui > .two > .field > #sylius_promotion_coupon_perCustomerUsageLimit').click();

    cy.get('.ui > .ui > .two > .field > #sylius_promotion_coupon_perCustomerUsageLimit').type('1');

    cy.get('.ui > .ui > .two > .field > #sylius_promotion_coupon_expiresAt').click();

    cy.get('.ui > .ui > .two > .field > #sylius_promotion_coupon_expiresAt').type('2023-06-21');

    cy.get('.admin-layout__content > .ui > .ui > .ui:nth-child(5) > .ui:nth-child(1)').click();

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_code_value').click();

    cy.get('.sylius-filters > .sylius-filters__field > .sylius-filters__group > .field > #criteria_code_value').type('FILTER');

    cy.get('.ui > .content > .ui > .ui:nth-child(2) > .icon').click();

    cy.get('tbody').children().should('have.length', 1);
  });
});
