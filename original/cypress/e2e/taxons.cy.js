function create_taxon() {
  cy.get('[id="sylius_taxon_code"]').type('new-taxon');
  cy.get('[id="sylius_taxon_translations_en_US_name"]').type('New Taxon');
  cy.get('[id="sylius_taxon_translations_en_US_slug"]').type('New Taxon slug');
  cy.get('[class="ui labeled icon primary button"]').scrollIntoView().click();
}

describe('taxons', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
    cy.clickInFirst('a[href="/admin/taxons/new"]');
  });
  // Remove .only and implement others test cases!
  it('edit dresses and see effect', () => {
    // Click in taxons in side menu
    cy.clickInFirst('a[href="/admin/taxons/new"]');
    // Click in dresses's more operations (...)
    cy.get('li:nth-child(3) > .sylius-tree__item > .sylius-tree__action > form > .ui > .ellipsis').click();
    // Click in edit item in dropdown
    cy.get('li:nth-child(3) > .sylius-tree__item > .sylius-tree__action > form > .ui > .menu > .item:nth-child(2)').click();
    // Edit dresses name
    cy.get('[id="sylius_taxon_translations_en_US_name"]').clear().type('Dresses Top');
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    cy.visit('/');
    // Assert that Dresses is renamed to Dresses Top
    cy.get('body').should('contain', 'Dresses Top');
  });

  it('create new taxon', () => {
    cy.get('[id="sylius_taxon_code"]').type('new-taxon');

    cy.get('*[class^="sylius-autocomplete ui fluid search selection dropdown"]').first().click();
    cy.get('div.item[data-value="mens_t_shirts"]').click();

    cy.get('[id="sylius_taxon_translations_en_US_name"]').type('New Taxon');
    cy.get('[id="sylius_taxon_translations_en_US_slug"]').type('New Taxon slug');
    cy.get('[class="ui labeled icon primary button"]').scrollIntoView().click();
    cy.get('body').should('contain', 'Taxon has been successfully created.');
  });

  it('check taxon hierarchy', () => {
    cy.get('.sylius-tree__item > .sylius-tree__action > form > .ui > .ellipsis').eq(1).click();
    cy.get('.sylius-tree__item > .sylius-tree__action > form > .ui > .menu').eq(1).should('be.visible');
  });
});
