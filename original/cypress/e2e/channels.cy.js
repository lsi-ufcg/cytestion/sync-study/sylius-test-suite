describe('channels', () => {
  beforeEach(() => {
    cy.visit('/admin');
    cy.get('[id="_username"]').type('sylius');
    cy.get('[id="_password"]').type('sylius');
    cy.get('.primary').click();
  });
  // Remove .only and implement others test cases!
  it('update the city of Fashion Web Store channel', () => {
    // Click in channels in side menu
    cy.clickInFirst('a[href="/admin/channels/"]');
    // Select only enabled channels
    cy.get('[id="criteria_enabled"]').select('Yes');
    // Click in filter blue button
    cy.get('*[class^="ui blue labeled icon button"]').click();
    // Click in edit of the last channel
    cy.get('*[class^="ui labeled icon button "]').last().click();
    // Change the country value
    cy.get('[id="sylius_channel_shopBillingData_city"]').scrollIntoView().clear().type('Town Town');
    // Click on Save changes button
    cy.get('[id="sylius_save_changes_button"]').scrollIntoView().click();

    // Assert that channel has been updated
    cy.get('body').should('contain', 'Channel has been successfully updated.');
  });

  it('test delete button', () => {
    cy.clickInFirst('a[href="/admin/channels/"]');
    cy.get('.buttons > form > .ui').click();
    cy.get('body').should('contain', 'Are you sure you want to perform this action?');
  });

  it('test create button', () => {
    cy.clickInFirst('a[href="/admin/channels/"]');
    cy.get('.right > .ui').click();
    cy.url().should('include', '/new');
  });

  it('test filter button', () => {
    cy.clickInFirst('a[href="/admin/channels/"]');
    cy.get('#criteria_search_type').select('Equal');
    cy.get('#criteria_search_value').type('FASHION_WEB');
    cy.get('.blue').click();
    cy.url().should('include', '/?criteria%5Bsearch%5D%5Btype%5D=equal&criteria%5Bsearch%5D%5Bvalue%5D=FASHION_WEB&criteria%5Benabled%5D=');
  });

  it('test delete all button', () => {
    cy.clickInFirst('a[href="/admin/channels/"]');
    cy.get('thead > tr > .center > input').click();
    cy.get('.sylius-grid-nav__bulk > form > .ui').click();
    cy.get('body').should('contain', 'Are you sure you want to perform this action?');
  });

  it('test filter button', () => {
    cy.clickInFirst('a[href="/admin/channels/"]');
    cy.get('#criteria_search_type').select('Equal');
    cy.get('#criteria_search_value').type('FASHION_WEB');
    cy.get('.blue').click();
    cy.get('.loadable > a.ui').click();
    cy.url().should('include', '');
  });
});
